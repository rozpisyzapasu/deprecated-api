require './lib/redcarpet_header_fix'
require './lib/api_helpers'

###
# API settings
###

set :googleAnalytics, 'UA-51106862-2'

###
# SLATE 
###
set :css_dir, 'assets/_css'
set :js_dir, 'assets/_js'
set :images_dir, 'assets/images'
set :fonts_dir, 'assets/fonts'
set :markdown_engine, :redcarpet

ignore 'includes/*'
ignore 'endpoint/*'

set :markdown, :fenced_code_blocks => true, :smartypants => true, :disable_indented_code_blocks => true, :prettify => true, :tables => true, :with_toc_data => true, :no_intra_emphasis => true

# Activate the syntax highlighter
activate :syntax

# This is needed for Github pages, since they're hosted on a subdomain
activate :relative_assets
set :relative_links, true

activate :api_helpers

# Build-specific configuration

configure :build do
  activate :minify_css, inline: true
  activate :minify_javascript, inline: true
  activate :minify_html
  activate :asset_hash
  activate :gzip
end

