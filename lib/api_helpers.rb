
class ApiHelpers < Middleman::Extension
  def initialize(app, options_hash={}, &block)
    super
  end

  helpers do
    def api_url(path)
      "#{data.api.endpoint}#{path}"
    end
  end
end

::Middleman::Extensions.register(:api_helpers, ApiHelpers)
